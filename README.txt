# ProjetWeb

Welcome in this quiz application. 

To use it you to create a condo environment, place you in the directory where the file manage.py is (directory mysite). 
When it's done run the command : python manage.py runserver, now the application is launch. 

I recommend you to create an account on the register page or if you already have one login you. 
If you just want see the gallery or the rating don't need to be login. 

On the home page you have a little description about the application and a button link to ranking of all players. The first have 
the best score.  

To start a quiz you need to be login because a score will be attribute for you. 
In this application two types of quiz are available : Microscopy and Component.

In the microscopy quiz you will have 3 pictures and 4 possibles answers, one is the good. If you find it, you win 1 point 
or if you have a wrong answer you loose 1 point. The good answer is the type of microscopy share between the pictures. 

In the component quiz you will have 2 pictures and 4 possibles answers, one is the good. If you find it, you win 3 points 
or if you have a wrong answer you loose 3 points. The good answer is the type of component share between the pictures.

In the gallery you have a table of each picture and the different informations about it, like : a description, component, microscopy,
doi... At the top a button link you to a page where you can sort the pictures by type of microscopy. 

When you have finish, logout you and stop the application by ctrl + c in your terminal. 

Thank you to download my application. 
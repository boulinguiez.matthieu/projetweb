# Generated by Django 3.0.3 on 2020-11-11 13:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Quizz', '0003_scoring'),
    ]

    operations = [
        migrations.AddField(
            model_name='scoring',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='scoring',
            name='id_usr',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user', to='Quizz.AuthUser'),
        ),
    ]

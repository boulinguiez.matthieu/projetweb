# Generated by Django 3.0.3 on 2020-11-11 12:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Quizz', '0002_authgroup_authgrouppermissions_authpermission_authuser_authusergroups_authuseruserpermissions_django'),
    ]

    operations = [
        migrations.CreateModel(
            name='Scoring',
            fields=[
                ('id_usr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='Quizz.AuthUser')),
                ('score', models.IntegerField(default=0)),
            ],
        ),
    ]

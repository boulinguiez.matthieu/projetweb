from django.urls import path

from . import views

urlpatterns = [
    path('home/', views.home),
    path('quizzHome/', views.quizzHome),
    path('microscopyQuizz/', views.microscopyQuizz),
    path('organelleQuizz/', views.organelleQuizz),
    path('login/', views.loginPage),
    path('register/', views.register),
    path('logout/', views.logoutPage),
    path('gallery/', views.gallery),
    path('rating/', views.ranking),
    path('gallery2/', views.autoBar, name='autocompleteBar'),
    path('microscopyQuizz/user/up/<int:identifiant>/<int:points>/', views.upScore),
    path('microscopyQuizz/user/down/<int:identifiant>/<int:points>/', views.downScore),
    path('organelleQuizz/user/up/<int:identifiant>/<int:points>/', views.upScore),
    path('organelleQuizz/user/down/<int:identifiant>/<int:points>/', views.downScore),
]

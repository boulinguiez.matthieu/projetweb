function CheckAnswer(buttonPick) {
    const main = document.createElement("div");
    main.setAttribute("id", "Main");

    const window = document.createElement("div");

    if (buttonPick.firstChild.nodeValue == "{{ answerTrue }}") {
        window.setAttribute("id", "Vrai");
        window.innerHTML += '<p> Ta réponse est juste </p>' ;
    } else {
        window.setAttribute("id", "Faux");
        window.innerHTML += '<p> Ta réponse est fausse </p>' ;
    }

    window.innerHTML += '<a href="http://127.0.0.1:8000/microscopyQuizz/"><button id="next" type="button">Next</button></a>' ;

    main.appendChild(window);

    const quizz = document.getElementById("content");
    document.body.insertBefore(main, quizz);
}
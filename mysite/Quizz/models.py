import random2
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User

class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.AutoField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    last_name = models.CharField(max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    action_flag = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class TableAnswer(models.Model):
    '''class of answers'''
    id_a = models.IntegerField(blank=True, null=True)
    q_id = models.IntegerField(blank=True, null=True)
    answer = models.TextField(blank=True, null=True)
    definition = models.TextField(blank=True, null=True)

    def randomAnswer(self):
        '''method to generate a good answer randomly'''
        answer = random2.randint(1, 4)
        return answer

    def moreInfo(self, reponse):
        '''method to return the more information'''
        definition = TableAnswer.objects.filter(id_a=reponse).values_list('definition', flat=True).first()
        return definition

    def ramdomOrga(self):
        '''method to get a random good answer'''
        all = TableAnswer.objects.filter(q_id=2)
        nb = random2.randint(0, len(all)-1)
        rand = all[nb]
        rep = rand.answer
        return rep

    def answerOrga(self, rep):
        '''method to return the other answer'''
        all = TableAnswer.objects.filter(q_id=2)
        # select pictures with q_id = 2
        listAns = []
        for reponse in all:
            if reponse.answer == rep:
            # add good pictures
                listAns.append(reponse)

        for reponse in all:
            if len(listAns) == 4:
                break
            elif reponse.answer != rep:
                if reponse not in listAns:
                # add wrong answer
                    listAns.append(reponse)

        random2.shuffle(listAns)
        # shuffle the list
        return listAns

    def orgaDescription(self, rep):
        '''method to return the more information'''
        description = TableAnswer.objects.filter(answer=rep).values_list('definition', flat=True).first()
        return description


class TableQuestion(models.Model):
    '''class of questions'''
    id_q = models.IntegerField(blank=True, null=True)
    question = models.TextField(blank=True, null=True)
    type_q = models.TextField(blank=True, null=True)
    imagefield = models.TextField(blank=True, null=True)
    point = models.IntegerField(blank=True, null=True)
    q_id = models.IntegerField(blank=True, null=True)
    n_answer = models.IntegerField(blank=True, null=True)
    n_image = models.IntegerField(blank=True, null=True)

class TablesImages(models.Model):
    '''class of pictures'''
    id_i = models.IntegerField(blank=True, null=True)
    name = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    mode = models.TextField(blank=True, null=True)
    celltype = models.TextField(blank=True, null=True)
    component = models.TextField(blank=True, null=True)
    doi = models.TextField(blank=True, null=True)
    organism = models.TextField(blank=True, null=True)

    def imagesMicoroscopy(self, reponse):
        '''method to create paths of each selected picture'''
        if reponse == 1:
            all = TablesImages.objects.filter(mode="fluorescence microscopy").values_list('name')
        elif reponse == 2:
            all = TablesImages.objects.filter(mode="scanning electron microscopy (SEM)").values_list('name')
        elif reponse == 3:
            all = TablesImages.objects.filter(mode="transmission electron microscopy (TEM)").values_list('name')
        elif reponse == 4:
            all = TablesImages.objects.filter(mode="phase contrast microscopy").values_list('name')

        taille = len(all)
        # number of pictures
        list_images = []
        while len(list_images) != 3:
        # 3 different random numbers
            alea = random2.randint(1, taille-1)
            # random
            if alea not in list_images:
            # if number not in list
                list_images.append(alea)
                # add to list

        list_imagesdb = []
        # list of pictures

        for i in list_images:
        # select good pictures according to random numbers
            list_imagesdb.append(all[i])
            # add to list

        list_chemin = []
        # list of paths
        for i in list_imagesdb:
        # create paths
            i = str(i).replace("(", "")
            i = str(i).replace(",)", "")
            chemin = "/images/" + i + ".jpg"
            list_chemin.append(chemin)
            # add path
        return list_chemin

    def answerT(self, reponse):
        '''method to return the good answer '''
        if reponse == 1:
            mode = "fluorescence microscopy"
        elif reponse == 2:
            mode = "scanning electron microscopy (SEM)"
        elif reponse == 3:
            mode = "transmission electron microscopy (TEM)"
        elif reponse == 4:
            mode = "phase contrast microscopy"
        return mode

    def imagesOrga(self, rep):
        '''method to create paths of pictures'''
        all = TablesImages.objects.filter(component=rep).values_list('name')
        # all pictures with the good component
        listRand = []
        while len(listRand) != 2:
        # generate 2 different random numbers
            rand = random2.randint(0, len(all)-1)
            # random
            if rand not in listRand:
                listRand.append(rand)
                # add to list

        listName = []
        for nb in listRand:
        # select pictures
            listName.append(all[nb])
            # add to list

        list_chemin = []
        for i in listName:
            # create paths
            i = str(i).replace("(", "")
            i = str(i).replace(",)", "")
            chemin = "/images/" + i + ".jpg"
            list_chemin.append(chemin)
            # add path
        return list_chemin

class Scoring(models.Model):
    '''class of scores'''
    id_usr = models.OneToOneField(User, related_name="user", on_delete=models.CASCADE)
    # id
    score = models.IntegerField(default=0)
    # score


@receiver(post_save, sender=User)
def createScoring(sender, instance, created, **kwargs):
    '''add score for each new player'''
    if created:
        Scoring.objects.create(id_usr=instance)
        # add score



from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from .form import CreateUserForm
from .models import TableAnswer, TableQuestion, TablesImages, Scoring, AuthUser
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

@login_required(login_url='http://127.0.0.1:8000/login/')
def home(request):
    '''Home page'''
    idd = request.user.id
    # keep user id
    return render(request, "HTMLfiles/acceuil.html",
                  {"goal" : Scoring.objects.filter(id_usr_id=idd).values_list('score', flat=True).first()})
    # goal : score of player

@login_required(login_url='http://127.0.0.1:8000/login/')
def quizzHome(request):
    '''Quiz home page'''
    idd = request.user.id
    # keep user id
    return  render(request, "HTMLfiles/quizzHome.html",
                   {"goal" : Scoring.objects.filter(id_usr_id=idd).values_list('score', flat=True).first()})
    # goal : score of player

@login_required(login_url='http://127.0.0.1:8000/login/')
def microscopyQuizz(request):
    '''Microscopy quiz page'''
    nombre = TableAnswer().randomAnswer()
    # select a number corresponding to the good answer
    idd = request.user.id
    # keep user id
    profil = get_object_or_404(Scoring, id_usr_id=idd)
    # keep player
    question = TableQuestion.objects.filter(id_q=1).values_list('question', flat=True).first()
    # keep question
    return render(request, "HTMLfiles/microscopyQuizz.html",
                   {"answers" : TableAnswer.objects.filter(q_id=1),
                    "quest" : question,
                    "pictures" : TablesImages().imagesMicoroscopy(nombre),
                    "answerTrue" : TablesImages().answerT(nombre),
                    "description" : TableAnswer().moreInfo(nombre),
                    "identifiant" : idd,
                    "goal" : profil.score})
    # answers : answer related to the question
    # quest : question
    # pictures : list of paths (for the pictures)
    # answerTrue : good answer
    # description : more informations
    # identifiant : player id
    # goal : score of player




@login_required(login_url='http://127.0.0.1:8000/login/')
def organelleQuizz(request):
    '''Component quiz page'''
    idd = request.user.id
    # keep user id
    profil = get_object_or_404(Scoring, id_usr_id=idd)
    # keep player
    answer = TableAnswer().ramdomOrga()
    # good answer
    return render(request, "HTMLfiles/organelleQuizz.html",
                   {"answers" : TableAnswer().answerOrga(rep=answer),
                    "quest" : TableQuestion.objects.filter(id_q=2).values_list('question', flat=True).first(),
                    "pictures" : TablesImages().imagesOrga(rep=answer),
                    "answerTrue" : answer,
                    "description" : TableAnswer().orgaDescription(rep=answer),
                    "identifiant": idd,
                    "goal" : profil.score})
    # answers : answer related to the question
    # quest : question
    # pictures : list of paths (for the pictures)
    # answerTrue : good answer
    # description : more informations
    # identifiant : player id
    # goal : score of player

def register(request):
    '''Register page'''
    if request.user.is_authenticated:
    # if player login
        return redirect('http://127.0.0.1:8000/home/')
        # go home
    else:
        form = CreateUserForm()
        # create empty form
        if request.method == 'POST':
        # check method
            form = CreateUserForm(request.POST)
            # create form with post method
            if form.is_valid():
            # if form valid
                form.save()
                # save form
                usr = form.cleaned_data.get('username')
                messages.success(request, "Account successfully for " + usr)
                # add message
                return redirect('http://127.0.0.1:8000/login/')
                # go to login page
        context = {'form' : form}
        return render(request, "HTMLfiles/register.html", context)

def loginPage(request):
    '''Login page'''
    if request.user.is_authenticated:
        # if player login
        return redirect('http://127.0.0.1:8000/home/')
        # go home
    else:
        if request.method == 'POST':
            # check method
            username = request.POST.get('username')
            # keep username
            password = request.POST.get('password')
            # keep pwd

            usr = authenticate(request, username=username, password=password)
            # connect player
            if usr is not None:
            # if player connect
                login(request, usr)
                # login him
                return redirect('http://127.0.0.1:8000/home/')
                # go home
            else:
                messages.info(request, 'Something is incorrect')

        return render(request, "HTMLfiles/login.html")

def logoutPage(request):
    '''Logout page'''
    logout(request)
    # logout player
    return redirect('http://127.0.0.1:8000/login/')
    # go login page

def gallery(request):
    '''Gallery page'''
    idd = request.user.id
    # keep player id
    profil = get_object_or_404(Scoring, id_usr_id=idd)
    # keep user
    return render(request, "HTMLfiles/gallery.html",
                  {"pictures" : TablesImages.objects.all(),
                   "goal" : profil.score})
    # pictures : list of pictures
    # goal : score of player

def ranking(request):
    '''Ranking page'''
    players = AuthUser.objects.all()
    # keep all players
    scores = Scoring.objects.all()
    # keep all scores
    idd = request.user.id
    # keep user id
    profil = get_object_or_404(Scoring, id_usr_id=idd)
    # keep player
    dictScores = {}
    # create dict
    for sc in scores:
        for player in players:
            if player.id == sc.id_usr_id:
                dicti = dict([(sc.score, player.username)])
                # add in dict a player and his score
            dictScores.update(dicti)

    listClass = []
    nb = 1
    for k, v in dictScores.items():
    # for each player
        string = "Position " + str(nb) + " : " + v + " (" + str(k) + "pts)"
        # create string of position
        listClass.append(string)
        # add to the list
        nb += 1

    return render(request, "HTMLfiles/rate.html",
                  {"list" : listClass,
                   "goal" : profil.score})
    # list : list of strings
    # goal : score of player

def autoBar(request):
    '''Sort gallery'''
    idd = request.user.id
    # keep user id
    profil = get_object_or_404(Scoring, id_usr_id=idd)
    # keep player
    if 'term' in request.GET:
        qs = TablesImages.objects.filter(mode__icontains=request.GET.get('term'))
        # take all possible mode
        images = list()
        for image in qs:
            if image.mode not in images:
                images.append(image.mode)
                # add in list
        return JsonResponse(images, safe=False)
    if request.method == 'POST':
    # check method
        word = request.POST.get("bar")
        # get text entry
        all = TablesImages.objects.filter(mode=word)
        # select good pictures
        return render(request, "HTMLfiles/gallery.html",
                      {"pictures" : all,
                       "goal" : profil.score})
        # pictures : list of pictures
        # goal : score of player
    return render(request, "HTMLfiles/autoBar.html",
                  {"goal" : profil.score})
    # goal : score of player

def upScore(request, identifiant, points):
    '''Add points'''
    usr = get_object_or_404(Scoring, id_usr_id=identifiant)
    # keep player
    usr.score += points
    # add points
    usr.save()
    # save user
    return JsonResponse(usr.score, safe=False)

def downScore(request, identifiant, points):
    '''Remove points'''
    usr = get_object_or_404(Scoring, id_usr_id=identifiant)
    # keep player
    usr.score -= points
    # remove points
    usr.save()
    # save user
    return JsonResponse(usr.score, safe=False)